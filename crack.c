#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=34;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *dictionaryFile = fopen(dictionary, "r");
    if (!dictionaryFile)
    {
        fprintf(stderr, "Can't open %s for reading.\n", dictionary);
        exit(1);
    }
    
    // Loop through the dictionary file, one line
    // at a time.
    // Hash each password. Compare to the target hash.
    // If they match, return the corresponding password.
    char *password = malloc(PASS_LEN * sizeof(char));
    while(fgets(password, PASS_LEN, dictionaryFile) != NULL)
    {
        char *nl = strchr(password, '\n');
        if (nl != NULL) *nl = '\0';
        char *hash = md5(password, strlen(password));
        if (strcmp(hash, target) == 0)
        {
            //printf("%s ", password);
            return password; 
        }
    }

    // Free up memory?
    free(password);
    fclose(dictionaryFile);
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hasheFile = fopen(argv[1], "r");
    if (!hasheFile)
    {
        fprintf(stderr, "Can't open %s for reading.\n", argv[1]);
        exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello 
    char hash[HASH_LEN];
    while(fgets(hash, HASH_LEN, hasheFile) != NULL)
    {
        char *nl = strchr(hash, '\n');
        if (nl != NULL) *nl = '\0';
        char *password = crackHash(hash, argv[2]);
        printf("%s %s\n", hash, password);
    }

    
    // Close the hash file
    fclose(hasheFile);
    
    // Free up any malloc'd memory?
    
    return 0;
}
